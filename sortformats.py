# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression.
Use the insertion algorithm"""

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """

    num1 = fordered.index(format1)
    num2 = fordered.index(format2)
    if num2 > num1:
        return True
    else:
        return False


def sort_pivot(formats: list, pivot: int):
    """Sort the pivot format, by exchanging it with the format
    on its left, until it gets ordered.
    """

    for pivot_pos in range(pivot, len(formats)):
        current_pos = pivot_pos
        while (current_pos > 0 < len(formats)):
            format1 = formats[current_pos-1]
            format2 = formats[current_pos]
            var = lower_than(format1, format2)
            if not var:
                formats[current_pos-1], formats[current_pos] = formats[current_pos], formats[current_pos-1]

            current_pos = current_pos-1
        while current_pos == len(formats):
            format1 = formats[current_pos]
            format2 = formats[current_pos-1]
            var = lower_than(format1, format2)
            if not var:
                formats[current_pos], formats[current_pos-1] = formats[current_pos-1], formats[current_pos]
    return formats


def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered. Use the insertion algorithm"""

    for pivote in range(len(formats)):
        pivote = formats.index(formats[1])
        sort_pivot(formats, pivote)
    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    sum2 = 0
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")

    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        sum2 += 1
        if sum2 == len(sorted_formats):
            print(format)
        else:
            print(format, end=" ")



if __name__ == '__main__':
    main()
